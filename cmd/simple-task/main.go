package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

const (
	defaultPort = "8080"

	htmlTemplate = `
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Simple Task</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">
</head>
<body>
	<div class="container">
	  <div class="row">
		  <div class="col-md-12">
			  <h1>Simple Task</h1>
			</div>
		</div>

	  <div class="row">
			<div class="col-md-12">
				<p>I'm currently running in an enivronment with hostname {{.Hostname}}<p>
			</div>
		</div>

	  <div class="row">
			<div class="col-md-8">
			  <h2>Environment</h2>
				<ul>
				{{ range .Env }}
				<li><pre>{{ . }}</li></pre>{{ end }}
				</ul>
			</div>
		</div>
	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
</body>
</html>
`
)

type simpleTask struct {
	Hostname string
	Env      []string
}

func main() {
	log.SetPrefix("simple-task: ")
	osPort := os.Getenv("PORT")
	if osPort == "" {
		osPort = defaultPort
	}

	port := fmt.Sprintf(":%s", osPort)
	log.Printf("listening on %q", port)

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(port, nil))

}

func handler(w http.ResponseWriter, r *http.Request) {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "i don't know"
	}

	st := simpleTask{
		Hostname: hostname,
		Env:      os.Environ(),
	}

	t := template.New("simple task")
	t.Parse(htmlTemplate)
	t.Execute(w, st)
}
